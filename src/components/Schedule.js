import React, { Component } from 'react';
import { Text, PickerIOS, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import axios from 'axios';
import ModalPicker from 'react-native-modal-picker';
import { Button } from './common';
import AddressList from './AddressList';

class Schedule extends Component {

  state = { slots: [], addresses: [], selected1: moment().format('DD-MM-YYYY') };

  componentWillMount() {
    this.getSlots(this.state.selected1);
  }
  onButtonPress() {

  }

  onValueChange(value: string) {
   this.setState({
     selected1: value
   });
   this.getSlots(value);
 }
  onValueChange2(value: string) {
  this.setState({
    selected2: value
  });
}
  onValueChange3(value: string) {
 this.setState({
   selected2: value
 });
}


  getSlots(val) {
    axios.get(`http://thewashapp.in/api/v1/customers/slots?date=${val}`, { headers: { 'Session-Token': this.props.session_token } })
      .then(response => this.setState({ slots: response.data.slots }))
        .catch((error) => console.log(error));

        return this.state.slots;
  }

  render() {
    return (

      <View style={{ flex: 3 }}>

        <View style={{ flex: 1 }}>

          <Text style={styles.TextStyle1}>
            Select type of services
          </Text>

        <View style={styles.ServicesViewStyle}>
            <View style={styles.ServiceTypeView}>
            <Text>48 hrs</Text>
            <TouchableOpacity style={styles.TouchableStyle}>
              <View style={styles.WrapperViewStyle}>
            <Image
              style={styles.ImgStyle}
              source={require('../Assets/Homepage/oicn_only iron.png')}
            /></View>
          </TouchableOpacity>
            <Text style={styles.TextStyle2}>IRON</Text>
            </View>

            <View style={styles.ServiceTypeView}>
            <Text>48 hrs</Text>
            <TouchableOpacity>
              <View style={styles.WrapperViewStyle}>
            <Image
              style={styles.ImgStyle}
              source={require('../Assets/Homepage/icn_fold.png')}
            /></View></TouchableOpacity>
              <Text style={styles.TextStyle2}>WASH & FOLD</Text>
            </View>

            <View style={styles.ServiceTypeView}>
            <Text>48 hrs</Text>
            <TouchableOpacity>
              <View style={styles.WrapperViewStyle}>
            <Image
              style={styles.ImgStyle}
              source={require('../Assets/Homepage/icn_wash_iron.png')}
            /></View></TouchableOpacity>
              <Text style={styles.TextStyle2}>WASH & IRON</Text>
            </View>

            <View style={styles.ServiceTypeView}>
            <Text>48 hrs</Text>
            <TouchableOpacity>
              <View style={styles.WrapperViewStyle}>
            <Image
              style={styles.ImgStyle}
              source={require('../Assets/Homepage/icn_dryclean.png')}
            /></View></TouchableOpacity>
              <Text style={styles.TextStyle2}>DRY CLEAN</Text>
            </View>

            <View style={styles.ServiceTypeView}>
            <Text>48 hrs</Text>
            <TouchableOpacity>
              <View style={styles.WrapperViewStyle}>
            <Image
              style={styles.ImgStyle}
              source={require('../Assets/Homepage/icn_premium.png')}
            /></View></TouchableOpacity>
              <Text style={styles.TextStyle2}>PREMIUM</Text>
            </View>

        </View>
        </View>

        <View style={{ flex: 1 }}>
        <Text style={styles.TextStyle3}>
          Schedule a pickup
        </Text>

        <View style={styles.PickerStyle}>
        <PickerIOS
          style={styles.DateStyle}
          selectedValue={this.state.selected1}
          onValueChange={this.onValueChange.bind(this)}
        >
              <PickerIOS.Item
                label={moment().format('DD-MM-YYYY')}
                value={moment().format('DD-MM-YYYY')}
              />
              <PickerIOS.Item
                label={moment().add(1, 'days').format('DD-MM-YYYY')}
                value={moment().add(1, 'days').format('DD-MM-YYYY')}
              />
              <PickerIOS.Item
                label={moment().add(2, 'days').format('DD-MM-YYYY')}
                value={moment().add(2, 'days').format('DD-MM-YYYY')}
              />
              <PickerIOS.Item
                label={moment().add(3, 'days').format('DD-MM-YYYY')}
                value={moment().add(3, 'days').format('DD-MM-YYYY')}
              />
              <PickerIOS.Item
                label={moment().add(4, 'days').format('DD-MM-YYYY')}
                value={moment().add(4, 'days').format('DD-MM-YYYY')}
              />
        </PickerIOS>

        <PickerIOS
          style={styles.PSlotStyle}
          selectedValue={this.state.selected2}
               onValueChange={this.onValueChange2.bind(this)}
        >
          {this.state.slots.map((slot, i) => {
             console.log('slot', slot);
            return (<PickerIOS.Item
              label={`${slot.starttime}-${slot.endtime}`}
              key={`${i}+1`} value={i}
            />);
           })}
        </PickerIOS>

        </View>

        </View>

        <View style={{ flex: 1 }}>
        <Text style={styles.TextStyle}>
          Pickup Address
        </Text>
        

        <Button
          onPress={this.onButtonPress.bind(this)}
        >
          Schedule Pickup
        </Button>
        </View>

        </View>

    );
  }
}

const styles = {
  TextStyle1: {
    fontSize: 16,
    alignSelf: 'flex-start',
    color: 'rgb(26, 25, 27)',
    paddingTop: 30,
    paddingLeft: 20,
    paddingBottom: 25
  },
  TextStyle2: {
    fontSize: 10,
    color: 'rgb(26, 25, 27)'
  },
  TextStyle3: {
    fontSize: 16,
    alignSelf: 'flex-start',
    color: 'rgb(26, 25, 27)',
    paddingLeft: 20
  },
  DateStyle: {
    width: 150
  },
  PSlotStyle: {
    width: 150
  },
  PickerStyle: {
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    flexDirection: 'row'
  },
  ImgStyle: {
    height: 30,
    width: 30,
    resizeMode: 'contain'
  },
  ServicesViewStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2
  },
  ServiceTypeView: {
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  WrapperViewStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  TouchableStyle: {

  }

};

const mapStateToProps = (state) => {
  const { session_token } = state.auth;
  return { session_token };
};

export default connect(mapStateToProps)(Schedule);
