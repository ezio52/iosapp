import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
} from './types';

export const emailChanged = (text) => ({
    type: EMAIL_CHANGED,
    payload: text
  });

export const passwordChanged = (text) => ({
    type: PASSWORD_CHANGED,
    payload: text
  });

export const loginUser = (fbId, accessToken) => (dispatch) => {
      dispatch({ type: LOGIN_USER });
      axios.post(`http://thewashapp.in/api/v1/customers/auth/facebook?fb_id=${fbId}&fb_access_token=${accessToken}`)
        .then(response => loginUserSuccess(dispatch, response.data.session_token))
          .catch(() => loginUserFail(dispatch));
  };

const loginUserFail = (dispatch) => {
    dispatch({ type: LOGIN_USER_FAIL });
  };

const loginUserSuccess = (dispatch, sessionToken) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: sessionToken
  });

  Actions.main();
};
