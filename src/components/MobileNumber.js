import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { CardSection, Input } from './common';

class MobileNumber extends Component {
  render() {
    return (
      <View>

        <CardSection>
          <Input
            label="Enter your Mobile Number"
            placeholder="9912225955"
            value={this.props.mobile}
            onChangeText={this.onMobileChange.bind(this)}
          />
        </CardSection>

      </View>
    );
  }
}


export default connect()(MobileNumber);
