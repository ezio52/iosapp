import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import index from './components/index';
import LoginForm from './components/LoginForm';

const RouterComponent = () => (
    <Router sceneStyle={{ paddingTop: 65 }}>

      <Scene key="auth">
        <Scene key="login" component={LoginForm} title="WashApp" />
      </Scene>

      <Scene key="main">
        <Scene key="index" component={index} title="WashApp" />

      </Scene>
    </Router>
  );

export default RouterComponent;
