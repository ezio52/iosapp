import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddressForm from './AddressForm';
import { addressUpdate, addressSave, addressDelete } from '../actions';
import { Card, CardSection, Button, Confirm } from './common';

class AddressEdit extends Component {
  state = { showModal: false };

  componentWillMount() {
    _.each(this.props.employee, (value, prop) => {
      this.props.addressUpdate({ prop, value });
    });
  }

  onButtonPress() {
    const { addresstype, flat, street, area } = this.props;

    this.props.addressSave({ addresstype, flat, street, area, uid: this.props.employee.uid });
  }


  onAccept() {
    const { uid } = this.props.employee;

    this.props.addressDelete({ uid });
  }

  onDecline() {
    this.setState({ showModal: false });
  }

  render() {
    return (
      <Card>
        <AddressForm />

        <CardSection>
          <Button onPress={this.onButtonPress.bind(this)}>
            Save Changes
          </Button>
        </CardSection>


        <CardSection>
          <Button onPress={() => this.setState({ showModal: !this.state.showModal })}>
            Fire Employee
          </Button>
        </CardSection>

        <Confirm
          visible={this.state.showModal}
          onAccept={this.onAccept.bind(this)}
          onDecline={this.onDecline.bind(this)}
        >
          Are you sure you want to delete this?
        </Confirm>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { addresstype, flat, street, area } = state.addressForm;

  return { addresstype, flat, street, area };
};

export default connect(mapStateToProps, {
  addressUpdate, addressSave, addressDelete
})(AddressEdit);
