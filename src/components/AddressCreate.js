import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addressUpdate, addressCreate } from '../actions';
import { Card, CardSection, Button } from './common';
import AddressForm from './AddressForm';

class AddressCreate extends Component {
  onButtonPress() {
    const { addresstype, flat, street, area } = this.props;

    this.props.addressCreate({ addresstype, flat, street, area });
  }

  render() {
    return (
      <Card>
        <AddressForm {...this.props} />
        <CardSection>
          <Button onPress={this.onButtonPress.bind(this)}>
            Create
          </Button>
        </CardSection>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { addresstype, flat, street, area } = state.addressCreateForm;

  return { addresstype, flat, street, area };
};

export default connect(mapStateToProps, {
  addressUpdate, addressCreate
})(AddressCreate);
