import {
  ADDRESS_UPDATE,
  ADDRESS_CREATE,
  ADDRESS_SAVE_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  address_type: '',
  flat: '',
  street: '',
  area: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADDRESS_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case ADDRESS_CREATE:
      return INITIAL_STATE;
    case ADDRESS_SAVE_SUCCESS:
      return INITIAL_STATE;
    default:
      return state;
  }
};
