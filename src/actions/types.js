export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const ADDRESS_UPDATE = 'address_update';
export const ADDRESS_CREATE = 'address_create';
export const ADDRESS_FETCH_SUCCESS = 'address_fetch_success';
export const ADDRESS_SAVE_SUCCESS = 'address_save_success';

export const GETSLOTS = 'getslots';
