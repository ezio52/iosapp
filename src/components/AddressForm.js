import React, { Component } from 'react';
import { View, Picker } from 'react-native';
import { connect } from 'react-redux';
import { addressUpdate } from '../actions';
import { CardSection, Input } from './common';

class AddressForm extends Component {
  render() {
    return (
      <View>
        <CardSection>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.props.addresstype}
            onValueChange={value => this.props.addressUpdate({ prop: 'addresstype', value })}
          >
            <Picker.Item label="Home" value="Home" />
            <Picker.Item label="Work" value="Work" />
            <Picker.Item label="Other" value="Other" />
          </Picker>
        </CardSection>

        <CardSection>
          <Input
            label="Flat / HouseNo"
            placeholder="401"
            value={this.props.flat}
            onChangeText={value => this.props.addressUpdate({ prop: 'flat', value })}
          />
        </CardSection>

        <CardSection>
          <Input
            label="Street"
            placeholder="Raghavendra Colony"
            value={this.props.street}
            onChangeText={value => this.props.addressUpdate({ prop: 'street', value })}
          />
        </CardSection>

        <CardSection style={{ flexDirection: 'column' }}>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.props.area}
            onValueChange={value => this.props.addressUpdate({ prop: 'area', value })}
          >
            <Picker.Item label="Madhapur" value="Madhapur" />
            <Picker.Item label="Gachibowli" value="Gachibowli" />
            <Picker.Item label="Kondapur" value="Kondapur" />
            <Picker.Item label="Manikonda" value="Manikonda" />
            <Picker.Item label="Hi-tech city" value="Hi-tech city" />
          </Picker>
        </CardSection>
      </View>
    );
  }
}


const mapStateToProps = (state) => {
  const { addresstype, flat, street, area } = state.addressForm;

  return { addresstype, flat, street, area };
};

export default connect(mapStateToProps, { addressUpdate })(AddressForm);
