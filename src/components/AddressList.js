import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import { addressFetch } from '../actions';
import ListItem from './ListItem';

class AddressList extends Component {
  componentWillMount() {
    this.props.addressFetch();

    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {
    // nextProps are the next set of props that this component
    // will be rendered with
    // this.props is still the old set of props

    this.createDataSource(nextProps);
  }

  createDataSource({ addresses }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(addresses);
  }

  renderRow(adress) {
    return <ListItem address={adress} />;
  }

  render() {
    return (
      <ListView
        enableEmptySections
        dataSource={this.dataSource}
        renderRow={this.renderRow}
      />
    );
  }
}

const mapStateToProps = state => {
  const { session_token } = state.auth;
  const addresses = _.map(state.addressForm, (val, uid) => ({ ...val, uid }));

  return { session_token, addresses };
};

export default connect(mapStateToProps, { addressFetch })(AddressList);
