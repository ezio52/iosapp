import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {

    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyDr8mPO8qLs-xelK4qHiouzdTkjBC6d5a0',
            authDomain: 'washapp-2f806.firebaseapp.com',
            databaseURL: 'https://washapp-2f806.firebaseio.com',
            projectId: 'washapp-2f806',
            storageBucket: 'washapp-2f806.appspot.com',
            messagingSenderId: '338418494240'
        };
        firebase.initializeApp(config);
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (

            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}


export default App;
