import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import FBSDK from 'react-native-fbsdk';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';

const { LoginButton, AccessToken } = FBSDK;

class LoginForm extends Component {
    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onButtonPress() {
        const { email, password } = this.props;

        this.props.loginUser({ email, password });
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }

        return (
            <Button onPress={this.onButtonPress.bind(this)}>
                Login
            </Button>
        );
    }

    renderFBButton() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }

        return (
          <LoginButton
          readPermissions={['public_profile']}
          onLoginFinished={(error, result) => {
            if (error) {
                console.log('login has error: ', result.error);
            } else if (result.isCancelled) {
                console.log('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then((data) => {
                     this.props.loginUser(data.userID, data.accessToken);
                });
            }
        }}
          />);
    }

    render() {
        return (

            <View>
                <Card>
                    <CardSection>
                        {this.renderFBButton()}
                    </CardSection>
                </Card>

                <Card>
                    <CardSection>
                        <Input
                          label="Email"
                          placeholder="email@gmail.com"
                          onChangeText={this.onEmailChange.bind(this)}
                          value={this.props.email}
                        />
                    </CardSection>

                    <CardSection>
                        <Input
                          secureTextEntry
                          label="Password"
                          placeholder="password"
                          onChangeText={this.onPasswordChange.bind(this)}
                          value={this.props.password}
                        />
                    </CardSection>

                    <Text style={styles.errorTextStyle}>
                        {this.props.error}
                    </Text>

                    <CardSection>
                        {this.renderButton()}
                    </CardSection>

                </Card>
            </View>

        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
};

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;

    return { email, password, error, loading };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
