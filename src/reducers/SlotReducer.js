import {
  GETSLOTS
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: '',
  session_token: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GETSLOTS:
      return { ...state, email: action.payload };
    default:
      return state;
  }
};
