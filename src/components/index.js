import React, { Component } from 'react';
import { TabBarIOS } from 'react-native';
import Schedule from './Schedule';
import AddressList from './AddressList';

class index extends Component {

  state = { selectedTab: 'schedule' };


  render() {
      return (
        <TabBarIOS
        selectedTab={this.state.selectedTab}
        >

        <TabBarIOS.Item
        title="Schedule"
        selected={this.state.selectedTab === 'schedule'}
        onPress={() => {
          this.setState({
            selectedTab: 'schedule',
          });
        }}
        >
        <Schedule />
        </TabBarIOS.Item>

        <TabBarIOS.Item
        title="Orders"
        selected={this.state.selectedTab === 'order'}
        onPress={() => {
          this.setState({
            selectedTab: 'order',
          });
        }}
        >
        <Schedule />
        </TabBarIOS.Item>

        <TabBarIOS.Item
        title="Services"
        selected={this.state.selectedTab === 'services'}
        onPress={() => {
          this.setState({
            selectedTab: 'services',
          });
        }}
        >
        <Schedule />
        </TabBarIOS.Item>


        </TabBarIOS>
      );
  }

}

export default index;
