import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {
  ADDRESS_UPDATE,
  ADDRESS_CREATE,
  ADDRESS_FETCH_SUCCESS,
  ADDRESS_SAVE_SUCCESS
} from './types';


export const addressUpdate = ({ prop, value }) => ({
    type: ADDRESS_UPDATE,
    payload: { prop, value }
  });

export const addressCreate = ({ addresstype, flat, street, area }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees`)
      .push({ addresstype, flat, street, area })
      .then(() => {
        dispatch({ type: ADDRESS_CREATE });
        Actions.employeeList({ type: 'reset' });
      });
  };
};

export const addressFetch = (sessionToken) => (dispatch) => {
    axios.get('http://thewashapp.in/api/v1/customers/addresses', { headers: { 'Session-Token': sessionToken } })
      .then((response) => {
        dispatch({ type: ADDRESS_FETCH_SUCCESS, payload: response.data.addresses });
      })
      .catch((error) => console.log(error));
  };

export const addressSave = ({ addresstype, flat, street, area, uid }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
      .set({ addresstype, flat, street, area })
      .then(() => {
        dispatch({ type: ADDRESS_SAVE_SUCCESS });
        Actions.employeeList({ type: 'reset' });
      });
  };
};

export const addressDelete = ({ uid }) => {
  const { currentUser } = firebase.auth();

  return () => {
    firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
      .remove()
      .then(() => {
        Actions.employeeList({ type: 'reset' });
      });
  };
};
